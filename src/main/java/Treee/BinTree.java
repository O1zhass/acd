package Treee;

public class BinTree {
    private Node root;


    public BinTree() {
        this.root = null;
    }

    public void insert(Integer key, String value) {
        Node node = new Node(key, value);
        Node current = root;
        if (root == null) {
            root = node;
        }
        while (current != null) {
            if (key > current.getKey()) {
                if (current.getRight() == null) {
                    current.setRight(node);
                    return;
                }
                current = current.getRight();
            } else if (key < current.getKey()) {
                if (current.getLeft() == null) {
                    current.setLeft(node);
                    return;
                }
                current = current.getLeft();
            }
        }

    }

    /*
    1 если корень ровно нулл  то вернуть корень
    2 если кей меньше кея

     */
    public boolean delete(Integer key) {
        del(this.root, key);
        return false;
    }

    private Node del(Node root, Integer key) {
        if (root == null) return root;
        if (key < root.getKey()) {
            root.setLeft(del(root.getLeft(), key));
        } else if (key > root.getKey()) {
            root.setRight(del(root.getRight(), key));
        }

        return root;
    }


    public String find(Integer key) {
        Node current = root;
        while (current != null) {
            if (key > current.getKey()) current = current.getRight();
            else if (key < current.getKey()) current = current.getLeft();
            else return current.getValue();
        }
        return null;
    }

    public void printAllAscending() {
        PaAsc(this.root);
    }

    private void PaAsc(Node root) {
        if (root == null) {
            return;
        }
        PaAsc(root.getLeft());
        System.out.print(root.getValue() + " ");
        PaAsc(root.getRight());
    }

    public void printAll() {


    }
}


