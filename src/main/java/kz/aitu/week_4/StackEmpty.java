package kz.aitu.week_4;
import java.util.Stack;

public class StackEmpty {
    public static class Empty {
        public static void main(String[] args) {


            Stack<Integer> stack = new Stack<Integer>();


            stack.push(12);
            stack.push(13);
            stack.push(14);
            stack.push(15);
            stack.push(16);


            System.out.println("The stack: " + stack);


            System.out.println("The stack empty? " + stack.empty());

        }
    }
}