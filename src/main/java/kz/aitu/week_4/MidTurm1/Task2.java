package kz.aitu.week_4.MidTurm1;

public class Task2 {
    public static class Node {
        String value;
        Node next;
        public Node(String value){
            this.value = value;
        }
    }
    public static class LinkedList {
        Node head;

        public boolean isEmpty(){
            return head == null;
        }

        public LinkedList(){
            this.head = null;
        }

        public void addTop(String value) {
            Node current = new Node(value);
            current.next = head;
            head = current;
        }

        public void removeAt(int position) {
            if (head == null) return;
            Node vrmm = head;
            if (position == 0){
                head = vrmm.next;
                return;
            }
            for (int ind = 0; vrmm != null && ind < position - 1; ind++)
                vrmm = vrmm.next;
            if (vrmm == null || vrmm.next == null)
                return;
            Node next = vrmm.next.next;
            vrmm.next = next;
        }

        public void showlist(){
            Node current = head;
            while (current.next != null) {
                System.out.println(current.value);
                current = current.next;
            }
            System.out.println(current.value);
        }

    }



}
