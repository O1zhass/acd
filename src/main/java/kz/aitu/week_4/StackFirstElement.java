package kz.aitu.week_4;

import java.util.Stack;

public class StackFirstElement {
    public static void main(String args[]) {

        Stack<Integer> stack = new Stack<Integer>();


        stack.add(12);
        stack.add(13);
        stack.add(14);
        stack.add(15);
        stack.add(16);


        System.out.println("Stack: " + stack);


        System.out.println("The first element is: "
                + stack.firstElement());
    }
}
