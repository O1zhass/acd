package kz.aitu.week_4.Heap.Stack.StackJava;

import java.util.Stack;
import java.util.Iterator;
import java.util.NoSuchElementException;

class StackClass {
    public static void main(String[] args){

    class Stack<Item> implements Iterable<Item> {
        private int n;
        private Node first;

        private class Node {
            private Item item;
            private Node next;
        }

        public Stack() {
            first = null;
            n = 0;
        }
        public boolean isEmpty() {
            return first == null;
        }
        public int size() {
            return n;
        }
        public void push(Item item) {
            Node oldfirst = first;
            first = new Node();
            first.item = item;
            first.next = oldfirst;
            n++;
        }

        public Item pop() {
            if (isEmpty()) throw new NoSuchElementException("Stack underflow");
            Item item = first.item;
            first = first.next;
            n--;
            return item;
        }

        public Item peek() {
            if (isEmpty()) throw new NoSuchElementException("Stack underflow");
            return first.item;
        }

        public String toString() {
            StringBuilder s = new StringBuilder();
            for (Item item : this) {
                s.append(item);
                s.append(' ');
            }
            return s.toString();
        }

        public Iterator<Item> iterator()  { return new ListIterator();  }
        private class ListIterator implements Iterator<Item> {
            private Node current = first;
            public boolean hasNext()  { return current != null;                     }
            public void remove()      { throw new UnsupportedOperationException();  }

            public Item next() {
                if (!hasNext()) throw new NoSuchElementException();
                Item item = current.item;
                current = current.next;
                return item;
            }
        }



        public void main(String[] args) {
            Stack<String> stack = new Stack<String>();
            while (!System.in.isEmpty()) {
                String item =System.in.readString();
                if (!item.equals("-")) stack.push(item);
                else if (!stack.isEmpty())   System.out.print(stack.pop() + " ");
            }
        System.out.println("(" + stack.size() + " left on stack)");
        }
    }
    }
}