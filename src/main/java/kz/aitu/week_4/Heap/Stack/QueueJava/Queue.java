package kz.aitu.week_4.Heap.Stack.QueueJava;

import java.util.Queue
import java.util.Iterator;
import java.util.NoSuchElementException;

class QueueClass {
    public static void main(String[] args){

         class Queue<Item> implements Iterable<Item> {
            private int n;
            private Node first;
            private Node last;


            private class Node {
                private Item item;
                private Node next;
            }


            public Queue() {
                first = null;
                last = null;
                n = 0;
            }


            public boolean isEmpty() {
                return first == null;
            }


            public int size() {
                return n;
            }


            public int length() {
                return n;
            }


            public Item peek() {
                if (isEmpty()) throw new NoSuchElementException("Queue underflow");
                return first.item;
            }


            public void enqueue(Item item) {
                Node oldlast = last;
                last = new Node();
                last.item = item;
                last.next = null;
                if (isEmpty()) first = last;
                else oldlast.next = last;
                n++;
            }


            public Item dequeue() {
                if (isEmpty()) throw new NoSuchElementException("Queue underflow");
                Item item = first.item;
                first = first.next;
                n--;
                if (isEmpty()) last = null;
                return item;
            }


            public String toString() {
                StringBuilder s = new StringBuilder();
                for (Item item : this)
                    s.append(item + " ");
                return s.toString();
            }



            public Iterator<Item> iterator() {
                return new ListIterator();
            }


            private class ListIterator implements Iterator<Item> {
                private Node current = first;

                public boolean hasNext() {
                    return current != null;
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }

                public Item next() {
                    if (!hasNext()) throw new NoSuchElementException();
                    Item item = current.item;
                    current = current.next;
                    return item;
                }
            }



            public  void main(String[] args) {
                Queue<String> queue = new Queue<String>();
                while (!StdIn.isEmpty()) {
                    String item = StdIn.readString();
                    if (!item.equals("-")) queue.enqueue(item);
                    else if (!queue.isEmpty()) StdOut.print(queue.dequeue() + " ");
                }
                StdOut.println("(" + queue.size() + " left on queue)");
            }
        }


}
}