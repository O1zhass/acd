package kz.aitu.week_4.Heap.Stack;
class StackJava {
    private Node first = null;

    private class Node
    {
        String data;
        Node next;
    }
   public boolean isEmpty()
   {
       return first == null;
   }
   public void push(String item) {
       Node oldfirst = first;

       first = new Node();

       first.next = oldfirst;
   }
   public String pop()
   {
       String data = first.data;
       first = first.next;
       return data;
   }
}
