package kz.aitu.week_3.linkedlist_example;

public class Main {

    public static void printGroup(Group group) {
        Block block = group.getFirst();

        while(block != null) {
            System.out.print(block.getValue() + " ");
            block = block.getNextBlock();
        }

        //block = 13
        //print
        //block = next => 7

        //block = 7
        //print
        //block = next => 3

        //block = 3
        //print
        //block = next => null

        //block = null => exit loop
    }

    public static void main(String[] args) {
        Group group1 = new Group();

        Block b1 = new Block();
        b1.setValue(13);
        b1.setNextBlock(null);

        Block b2 = new Block();
        b2.setValue(7);
        b2.setNextBlock(null);

        Block b3 = new Block();
        b3.setValue(3);
        b3.setNextBlock(null);

        group1.addBlock(b1);
        group1.addBlock(b2);
        group1.addBlock(b3);

        printGroup(group1);
    }
}
