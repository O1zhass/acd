package kz.aitu.week_3.ls_cs03;

import lombok.Data;

@Data
public class Node {
    private double value = 0.0;
    private Node nextNode = null;

    public Node(double value) {
        this.value = value;
    }
}
