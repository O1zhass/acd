package kz.aitu.week1;

import java.util.Scanner;

public class Practice8 {

    int c = 0;

    long h[] = new long[1000];

    public long fin1(int n) {
        if(n == 0 || n == 1) {
            h[n] = n;
            return n;
        }

        System.out.println(c++);

        long a, b;
        if(h[n-1] != 0) {
            a = h[n-1];
        }
        else {
            a = fin1(n-1);
            h[n-1] = a;
        }

        if(h[n-2] != 0) {
            b = h[n-2];
        }
        else {
            b = fin1(n-2);
            h[n-2] = b;
        }

        return a+b;
    }

    public long fin3(int n) {
        if(n == 0 || n == 1) {
            h[n] = n;
            return n;
        }
        System.out.println(c++);

        return fin3(n-1) + fin3(n-2);
    }

    public long fin2(int n) {
        h[0] = 0;
        h[1] = 1;

        for (int i = 2; i <= n; i++) {
            h[i] = h[i-1] + h[i-2];
            //System.out.println(i + "> " + h[i]);
        }
        return h[n];
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        System.out.println(fin1(n));
    }
}
