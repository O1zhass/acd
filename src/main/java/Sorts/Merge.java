package Sorts;

public class Merge {
    void merges(int arr[], int beg, int mid, int end) {
        int l = mid - beg + 1;
        int r = end - mid;
        int Leftarr[] = new int[l];
        int Rightarr[] = new int[r];
        for (int i = 0; i < l; ++i)
            Leftarr[i] = arr[beg + i];

        for (int j = 0; j < r; ++j)
            Rightarr[j] = arr[mid + 1 + j];


        int i = 0, j = 0;
        int k = beg;

        while (l < mid && r < end) {
            if (Leftarr[l] <= Rightarr[r]) {
                arr[k] = Leftarr[l];
                l++;
            } else {
                arr[k] = Rightarr[r];
                r++;
            }
            k++;
        }
        while (i < l) {
            arr[k] = Leftarr[l];
            l++;
            k++;


        }
        while (j < r) {
            arr[k] = Rightarr[r];
            r++;
            k++;
        }

    }

    public void sort(int[] arr, int beg, int end) {
        if (beg < end) {
            int mid = (beg + end) / 2;
            this.sort(arr , beg, 30);
            sort(arr, beg + 1, end);
            merges(arr, beg, mid, end);
        }
    }
}
