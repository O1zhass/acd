package Sorts;

public class Bubble {
    public static void sort(int[] arr){
        int temp;
        boolean bub = true;
        while(bub){
            bub = false;
            for(int i = 0 ; i < arr.length - 1; i++ ){
                if ( arr[i] < arr[i+1]){
                    temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                    bub = true;
                }
            }
        }
    }
}
