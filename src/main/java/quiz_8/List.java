package quiz_8;

public class List {

    private Student[] studentList;
    private int size = 0;

    public List() {
        studentList = new Student[150];
    }

    public void print() {
        for (int i = 0; i < size; i++) {
            System.out.println(studentList[i]);
        }
    }

    public void add(Student student) {
        for (int i = 0; i < size; i++) {
            studentList[i] = student;
        }
        size++;
    }

    public void sort(String type, String order) {
        if (type.equals("name_a") && order.equals("asc")) {
            // sort_name_a_asc();
        } else if (type.equals("name_a") && order.equals("desc")) {
            // sort_name_a_asc(;);
            // sort_name_a_desc()
        } else if (type.equals("name_l") && order.equals("asc")) {
            // sort_name_a_asc();
            // sort_name_l_asc();
        } else if (type.equals("name_l") && order.equals("desc")) {
            //sort_name_a_asc();
            // sort_name_l_desc();
        } else if (type.equals("age") && order.equals("asc")) {
            // sort_name_a_asc();
            sort_age_asc();
        } else if (type.equals("age") && order.equals("desc")) {
            //  sort_name_a_asc();
            sort_age_desc();
        } else if (type.equals("gpa") && order.equals("asc")) {
            // sort_name_a_asc();
            sort_gpa_asc();
        } else if (type.equals("gpa") && order.equals("desc")) {
            //sort_name_a_asc();
            //sort_gpa_desc();
        }
    }

    public void sort_gpa_asc() {
        for (int i = 0; i < studentList.length - 1; i++) {
            int maxIndex = i;
            double temp;
            for (int j = i + 1; j < studentList.length; j++) {
                if (studentList[j] != null) {
                    if (studentList[j].getGpa() < studentList[maxIndex].getGpa()) {
                        maxIndex = j;
                    }
                    temp = studentList[maxIndex].getGpa();
                    studentList[maxIndex].setGpa(studentList[i].getGpa());
                    studentList[i].setGpa(temp);
                }
            }
        }
    }

    public  void sort_age_desc() {
        for (int i = 0; i < studentList.length - 1; i++) {
            int minIndex = i;
            Student temp;
            for (int j = i + 1; j < studentList.length; j++) {
                if (studentList[j] != null) {
                    if (studentList[j].getAge() > studentList[minIndex].getAge()) {
                        minIndex = j;
                    }
                    temp = studentList[minIndex];
                    studentList[minIndex] = studentList[i];
                    studentList[i] = temp;
                }
            }
        }

    }


    public void sort_name_a_desc() {

    }

    public void sort_name_l_asc() {

    }

    public void sort_name_l_desc() {

    }

    public void sort_age_asc() {
        for (int i = 0; i < studentList.length - 1; i++) {
            int maxIndex = i;
            Student temp;
            for (int j = i + 1; j < studentList.length; j++) {
                if (studentList[j] != null) {
                    if (studentList[j].getAge() < studentList[maxIndex].getAge()) {
                        maxIndex = j;
                    }
                    temp = studentList[maxIndex];
                    studentList[maxIndex] = studentList[i];
                    studentList[i] = temp;
                }


            }
        }
    }
}