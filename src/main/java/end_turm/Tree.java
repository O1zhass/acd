package end_turm;

public class Tree {
    Node root;

    public void insert(Integer key, String value) {
        Node node = new Node(key, value);

        if (root == null) {
            root = node;
        } else {
            Node current = root;
            while (current != null) {
                if (key < current.getKey()) {
                    if (current.getLeft() == null) {
                        current.setLeft(node);
                        return;
                    }
                    current = current.getLeft();
                } else if (key > current.getKey()) {
                    if (current.getRight() == null) {
                        current.setRight(node);
                        return;
                    }
                    current = current.getRight();
                }

            }
        }
    }

    public Node countOdd(Node root) {
        if (root != null) {
            countOdd(root.getLeft());
            if (root.getKey() % 2 != 0)
                System.out.print(root.getKey() + " ");
            countOdd(root.getRight());
        }
        return root;
    }

    public Node ica(Node root, int value1, int value2) {

        return root;
    }

    public int Minfind() {
        Node current = root;
        Node i = null;
        while (current != null) {
            i = current;
            current = current.getLeft();
        }
        return i.getKey();
    }

}
