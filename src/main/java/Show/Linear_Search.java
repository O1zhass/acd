package Show;

import java.util.Scanner;

public class Linear_Search {
    public static void main(String[] args) {
        int key = 0;
        int value;
        int[] arr = {15, 12, 35, 24, 25, 16, 27, 18, 29, 10, 11};
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Value ?");
        value = sc.nextInt();
        for (int i = 0; i < 10; i++) {
            if (value == arr[i]) {
                key = i + 1;
                break;
            } else {
                key = 0;
            }
        }
        if (value != 0) {
            System.out.println("Value found at location " + key);
        } else {
            System.out.println("Value not found");
        }

    }
}
